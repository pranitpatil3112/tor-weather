from typing import Any

from tor_weather.core.smtp import Smtp
from tor_weather.jobs.classes.relay import RelayObject
from tor_weather.jobs.classes.subscription import SubscriptionObject


class SubscriptionJob:
    """Class Representing a Subscription"""

    def __init__(
        self,
        subscription_obj: SubscriptionObject,
        relay_obj: RelayObject,
        flag=None,
    ) -> None:
        self.subscription: SubscriptionObject = subscription_obj
        self.relay: RelayObject = relay_obj
        self.email_class = None
        self.flag = flag

    def set_child_subscription(self, child_sub: str) -> None:
        """
        Set name of child subscription
        """
        self.subscription.parse(child_sub)

    def _generate_email(self, data: Any):
        """Generated an email template

        Args:
            data (_type_): Data for generating the email template

        Returns:
            _type_: Generated email object
        """
        subscriber_email = self.subscription.get_subscriber().email
        return self.email_class(subscriber_email, data)  # type: ignore

    def send_email(self, data) -> None:
        """Sends an email

        Args:
            data (_type_): Email object
        """
        email_template = self._generate_email(data)
        Smtp().send_mail(email_template)
