from datetime import datetime
from typing import Any, Type

from tor_weather.core.mail import NodeBandwidthEmail
from tor_weather.extensions import job_logger
from tor_weather.jobs.classes.relay import RelayObject
from tor_weather.jobs.classes.subscription import SubscriptionObject
from tor_weather.jobs.subscriptions.subscription_job import SubscriptionJob
from tor_weather.utilities import time_diff


class NodeBandwidthSubscriptionJob(SubscriptionJob):
    def __init__(self, subscription_obj: SubscriptionObject, relay_obj: RelayObject):
        super().__init__(subscription_obj, relay_obj)
        self.email_class: Type[NodeBandwidthEmail] = NodeBandwidthEmail  # type: ignore
        self.child_sub_name: str = "node_bandwidth_sub"
        self.set_child_subscription(self.child_sub_name)

    def get_data_for_mail(self) -> Any:
        return {
            "fingerprint": self.relay.fingerprint,
            "wait_for": self.subscription.get_value("wait_for"),
            "threshold": self.subscription.get_value("threshold"),
        }

    def validate(self) -> None:
        if self.subscription.child_sub and self.subscription.get_value("is_active"):
            target_bandwidth = self.subscription.get_value("threshold")
            observed_bandwidth = self.relay.bandwidth["observed"]
            if not self.subscription.get_value("issue_first_seen"):
                # Relay was previously running fine on bandwidth
                if observed_bandwidth < target_bandwidth:
                    # Relay was running fine & is now running low on bandwidth
                    self.subscription.set_value("issue_first_seen", datetime.utcnow())
            else:
                # Relay was previously running low on bandwidth
                if not observed_bandwidth < target_bandwidth:
                    # Relay was running low on bandwidth & just started on right bandwidth
                    self.subscription.set_value("issue_first_seen", None)
                    self.subscription.set_value("emailed", False)
                else:
                    # Relay was running low & is running low on bandwidth
                    issue_first_seen = self.subscription.get_value("issue_first_seen")
                    wait_for = self.subscription.get_value("wait_for")
                    if time_diff(issue_first_seen, datetime.utcnow()) >= wait_for:
                        # Relay has been running low on bandwidth for more than the waiting time
                        if not self.subscription.get_value("emailed"):
                            # Subscriber was not sent an email already
                            mail_data = self.get_data_for_mail()
                            try:
                                self.send_email(mail_data)
                                self.subscription.set_value("emailed", True)
                            except Exception as e:
                                job_logger.exception(f"Failed to send an email - {e}")
            self.subscription.commit()
