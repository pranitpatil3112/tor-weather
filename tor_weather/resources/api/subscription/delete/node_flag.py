from typing import cast

from flask_login import current_user, login_required
from flask_restx import Resource

from tor_weather.core.database.tables.subscriber import Subscriber
from tor_weather.routes import api_ns
from tor_weather.service.subscription.node_flag import NodeFlagService


class NodeFlagSubscriptionDeleteApi(Resource):
    """Implements the Node Flag Subscription Delete API"""

    @login_required
    @api_ns().response(302, "Redirect to the Subscription List Page")
    def get(self, flag_name: str, fingerprint: str):
        subscriber = cast(Subscriber, current_user)
        return NodeFlagService(subscriber.email, flag_name).delete_subscription(
            fingerprint
        )
