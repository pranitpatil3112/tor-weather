from flask.wrappers import Response
from flask_restx import Resource

from tor_weather.error import BadRequest
from tor_weather.routes import api_ns
from tor_weather.service.user import User

login_parser = api_ns().parser()
login_parser.add_argument("email", type=str, location="form", required=True)
login_parser.add_argument("password", type=str, location="form", required=True)


class LoginApi(Resource):
    """Implements API for Login"""

    @api_ns().doc(parser=login_parser)
    @api_ns().response(302, "Redirect to Dashboard or Login Screen")
    @api_ns().response(400, "Incorrect Input")
    def post(self) -> Response:
        args = login_parser.parse_args()
        if args["email"] and args["password"]:
            return User().login(args["email"], args["password"])
        else:
            raise BadRequest("Required inputs not passed")
