from tor_weather.constants import TABLE_HEADER_DISPLAY_NAME

# The Subscription Class

# Common Methods:
# 1. Should get the list of all subscriptions for the current user from the database
# 2. Should get the subscription object containing the fingerprint and the child subscription object


class SubscriptionService:

    TABLE_HEADER_KEYS: list[str]
    TABLE_DAO = None

    def __init__(self, email: str, flag: str = None):
        self.email = email
        self.flag = flag

    def _get_modify_url(self, id: int):
        pass

    def _get_delete_url(self, id: str):
        pass

    def _get_enable_url(self, id: str):
        pass

    def _get_disable_url(self, id: str):
        pass

    def _get_table_header(self):
        table_headers = []
        for key in self.TABLE_HEADER_KEYS:
            header_obj = {
                "display_name": TABLE_HEADER_DISPLAY_NAME.get(key),
                "value": key,
            }
            table_headers.append(header_obj)
        return table_headers

    def _create_table_cell(self, display_name: str = None, url: str = None):
        table_cell_object = {}
        if display_name is not None:
            table_cell_object["display_name"] = display_name

        if url is not None:
            table_cell_object["url"] = url

        return table_cell_object

    def _get_table_content(self):
        subscriptions = self.TABLE_DAO(
            email=self.email, flag=self.flag
        ).get_subscriptions()
        table_rows = []
        for subscription in subscriptions:
            fingerprint = subscription.get("fingerprint")
            table_row = {}
            for key in self.TABLE_HEADER_KEYS:
                if key == "fingerprint":
                    value = fingerprint
                    table_cell = self._create_table_cell(display_name=value)
                    table_row[key] = table_cell
                else:
                    value = getattr(subscription.get("child"), key)
                    table_cell = self._create_table_cell(display_name=value)
                    table_row[key] = table_cell
            # Add a cell for the edit button details
            table_edit_url = self._get_modify_url(fingerprint)
            table_edit_cell = self._create_table_cell(url=table_edit_url)
            table_row["edit_url"] = table_edit_cell
            # Add a cell for the delete button details
            table_delete_url = self._get_delete_url(fingerprint)
            table_delete_cell = self._create_table_cell(url=table_delete_url)
            table_row["delete_url"] = table_delete_cell
            table_rows.append(table_row)
        return table_rows

    def get_subscription(self):
        pass
