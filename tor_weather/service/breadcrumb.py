BREADCRUMB_DATA = {
    "categories": [
        {
            "displayName": "Node Status",
            "options": [
                {
                    "displayName": "Node Down",
                    "url": "/dashboard/node-status/node-down",
                    "icon": "trending_down",
                },
                {
                    "displayName": "Node Bandwidth",
                    "url": "/dashboard/node-status/node-bandwidth",
                    "icon": "speed",
                },
            ],
        },
        {
            "displayName": "Node Flag",
            "options": [
                {
                    "displayName": "Node Exit Flag",
                    "url": "/dashboard/node-flag/exit",
                    "icon": "door_back",
                },
                {
                    "displayName": "Node Stable Flag",
                    "url": "/dashboard/node-flag/stable",
                    "icon": "done_all",
                },
                {
                    "displayName": "Node Guard Flag",
                    "url": "/dashboard/node-flag/guard",
                    "icon": "shield",
                },
                {
                    "displayName": "Node Valid Flag",
                    "url": "/dashboard/node-flag/valid",
                    "icon": "verified",
                },
                {
                    "displayName": "Node Fast Flag",
                    "url": "/dashboard/node-flag/fast",
                    "icon": "bolt",
                },
            ],
        },
        {
            "displayName": "Support",
            "options": [
                {
                    "displayName": "Get Help",
                    "url": "/get-help",
                    "icon": "info",
                },
                {
                    "displayName": "Submit Feedback",
                    "url": "/submit-feedback",
                    "icon": "chat_bubble_outline",
                },
                {"displayName": "Logout", "url": "/logout", "icon": "logout"},
            ],
        },
    ]
}


class Breadcrumb:
    def __init__(self, route: str):
        self.route = route

    def _get_category_for_option(self):
        for subscription in BREADCRUMB_DATA["categories"]:
            for option in subscription["options"]:
                if option["url"] in self.route:
                    return [subscription["displayName"], option["displayName"]]

    def get_data(self):
        breadcrumb_data = ["Subscriptions"]
        breadcrumb_data.extend(self._get_category_for_option())
        if self.route[-6:] == "modify":
            breadcrumb_data.append("Modify")
        elif self.route[-6:] == "create":
            breadcrumb_data.append("Create")
        return breadcrumb_data
