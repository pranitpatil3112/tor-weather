# from tor_weather.core.smtp import Smtp
from typing import Any

from flask import flash, make_response, redirect, url_for
from flask.wrappers import Response
from flask_login import login_user
from sqlalchemy import exc

from tor_weather.constants import SnackbarMessage
from tor_weather.core.database import Subscriber
from tor_weather.core.mail import AccountVerificationEmail
from tor_weather.core.smtp import Smtp
from tor_weather.env import get_env
from tor_weather.error import BadRequest, InternalError
from tor_weather.extensions import db
from tor_weather.utilities.encryption import decrypt, encrypt
from tor_weather.utilities.hash import check_password, hash_password


class User:
    def __init__(self) -> None:
        pass

    @staticmethod
    def _get_user_from_database(email: str) -> Subscriber:
        """Returns the user from the database

        Args:
            email (str): Email of the user to be extracted from the database

        Returns:
            _type_: User from the database
        """
        if email:
            return Subscriber.query.filter_by(email=email).first()
        else:
            raise BadRequest("Email is not defined")

    @staticmethod
    def _generate_email_verification_code(email: str) -> str:
        """Generated an encrypted code for email verification

        Args:
            email (str): Email for which to generate the verification code

        Returns:
            str: Email Verification Code
        """
        email_verification_code = encrypt(email)
        return email_verification_code

    @staticmethod
    def _add_to_database(email: str, password: str) -> None:
        """Adds the user to the database

        Args:
            email (str): Email of the user
            password (str): Password of the user
        """
        hashed_pass = hash_password(password)
        subscriber = Subscriber(email=email, password=hashed_pass)  # type: ignore
        db.session.add(subscriber)
        try:
            db.session.commit()
        except exc.SQLAlchemyError:
            db.session.rollback()
            raise InternalError()

    def _get_data_for_mail(self, verification_code: str) -> Any:
        return {
            "verification_code": verification_code,
            "base_url": get_env("BASE_URL"),
        }

    def _send_email_verification_code(self, email: str) -> None:
        """Sends an email with the verification code for the signup

        Args:
            email (str): Email of the user to send the verification email to.
        """
        verification_code = self._generate_email_verification_code(email)
        mail_data = self._get_data_for_mail(verification_code)
        mail_content = AccountVerificationEmail(email, mail_data)
        Smtp().send_mail(mail_content)

    def register(self, email: str, password: str) -> Response:
        """Register a new user to the service

        Args:
            email (str): Email of the user
            password (str): Password of the user

        Returns:
            _type_: A response object
        """
        database_user = self._get_user_from_database(email)
        if database_user:
            flash(**SnackbarMessage.USER_ALREADY_EXISTS.value)
            return make_response(redirect(url_for("login")))
        else:
            self._add_to_database(email, password)
            self._send_email_verification_code(email)
            flash(**SnackbarMessage.VERIFICATION_EMAIL_SENT.value)
            return make_response(redirect(url_for("login")))

    def login(self, email: str, password: str) -> Response:
        """Verify the credentials for the user

        Args:
            email (str): Email of the user
            password (str): Password of the user

        Returns:
            Response: A response object
        """
        database_user = self._get_user_from_database(email)
        if database_user:
            passwordMatch = check_password(password, database_user.password)
            accountVerified = database_user.is_confirmed
            if passwordMatch and accountVerified:
                login_user(database_user)
                return make_response(redirect(url_for("dashboard_home")))
            elif passwordMatch:
                self._send_email_verification_code(email)
                flash(**SnackbarMessage.VERIFICATION_EMAIL_RESEND.value)
                return make_response(redirect(url_for("login")))
            else:
                flash(**SnackbarMessage.INCORRECT_CREDENTIALS.value)
                return make_response(redirect(url_for("login")))
        else:
            flash(**SnackbarMessage.USER_NOT_FOUND.value)
            return make_response(redirect(url_for("register")))

    def verify_email(self, verification_code: str) -> Response:
        """Verify the user with the verification code from the email

        Args:
            verificationCode (str): Verification Code sent to the user's email

        Returns:
            _type_: A response object
        """
        user_email = decrypt(verification_code)
        database_user = self._get_user_from_database(user_email)
        if database_user:
            if database_user.is_confirmed:
                flash(**SnackbarMessage.EMAIL_VERIFICATION_REPEAT.value)
                return make_response(redirect(url_for("login")))
            else:
                database_user.is_confirmed = True
                try:
                    db.session.commit()
                    flash(**SnackbarMessage.EMAIL_VERIFICATION_SUCCESS.value)
                    return make_response(redirect(url_for("login")))
                except exc.SQLAlchemyError:
                    db.session.rollback()
                    raise InternalError()
        else:
            raise BadRequest("Incorrect verification-code")
