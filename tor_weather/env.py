from dotenv import load_dotenv
from flask import Flask

"""Stores the environment variables for the application"""
env: dict[str, str] = {}


def configure_env(app: Flask) -> None:
    """Stores a local copy for environment variables for the application

    Args:
        app (Flask): The current app instance
    """
    # 1. Load the environment variables to the OS
    load_dotenv()
    # 2. Assign environment variables from OS to App
    app.config.from_pyfile("./settings.py")
    # 3. Store a copy of environment variables to be imported across the application
    global env
    env = dict(app.config)


def get_env(key: str) -> str:
    """Get the environment variable value

    Args:
        key (str): Name of key

    Returns:
        str: Value from environment variables
    """
    value = env.get(key)
    if value is not None:
        return value
    else:
        raise KeyError(f"{key} does not exist")
